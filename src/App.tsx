import Container from "@material-ui/core/Container";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  BrowserRouter
} from "react-router-dom";
import { Noticias } from "./components/consultar/noticias/Noticias";
import { Navigation } from "./components/router/Navigation";
import { Tiempo } from './components/consultar/tiempo/Tiempo';
import { Historial } from './components/consultar/Historial';

function App() {

  /* const [categories, setCategories] = useState(['']); */

  return (
    <Container maxWidth="sm">
      <BrowserRouter>
        <Navigation />
        <hr/>
        <Switch>
          <Route exact path="/">
            <Noticias />
          </Route>
          <Route exact path="/tiempo">
            <Tiempo />
          </Route>
          <Route path="/historial">
            <Historial />
          </Route>
        </Switch>
      </BrowserRouter>
    </Container>
  );
}

export default App;
