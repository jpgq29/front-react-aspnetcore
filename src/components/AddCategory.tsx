import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import React, { useState } from 'react';

interface Props {
    setCategories: any
}

export const AddCategory = ({ setCategories }: Props) => {
    const [inputValue, setInputValue] = useState('')

    const handleInputChange = (e: any) => {
        setInputValue(e.target.value);
    }

    const handleInputSubmit = (e: any) => {
        e.preventDefault();
        if (inputValue.trim().length > 2) {
            setCategories(inputValue); 
            setInputValue('');
        }
    }

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <form onSubmit={handleInputSubmit}>
                    <FormControl fullWidth>
                        <TextField
                            id="standard-basic"
                            label="Agregue una palabra por buscar..."
                            value={inputValue}
                            onChange={handleInputChange} />
                    </FormControl>
                </form>
            </Grid>
        </Grid>
    )
}
