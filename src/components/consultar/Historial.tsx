import React, { useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';

export const Historial = () => {

    const [datos, setData] = useState([]);

    useEffect(() => {
        gifGrid();
    }, [])

    const gifGrid = async () => {


        const url = `http://192.168.0.10:5000/api/v1/historial`;

        try {
            const { data } = await axios.get(url);
            setData(data);           
        } catch (error) { }

    }

    const columns = [
        { field: 'id', headerName: 'ID', width: 90 },
        {
            field: 'palabra',
            headerName: 'Keyword',
            width: 150,
            editable: true,
        },
        {
            field: 'tipo',
            headerName: 'Tipo',
            width: 150,
            editable: true,
        },
        {
            field: 'created_at',
            headerName: 'Fecha registro',
            width: 150,
            editable: true,
        },
    ];

    return (
        <div style={{ height: 400, width: '100%' }}>
            <DataGrid
                rows={datos}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                checkboxSelection
                disableSelectionOnClick
            />
        </div>
    )
}
