import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

interface Props {
    items: any;
}

export const NoticiasGridItem = ({ items }: Props) => {
    const { title, description, url, urlToImage } = items;
    return (
        <Grid item xs={6}>
            <Card>
                <CardActionArea
                    href={url}
                    target="_blank"
                >

                    <CardMedia
                        component="img"
                        height="140"
                        image={urlToImage}
                        title={title}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    )
}
