import { useEffect, useState } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import { NoticiasGridItem } from './NoticiasGridItem';

interface Props {
    category: string
}

export const NoticiasGrid = ({ category }: Props) => {

    const [datos, setData] = useState([]);

    useEffect(() => {
        gifGrid();
    }, [category])

    const gifGrid = async () => {

        if (category.length > 0) {
            const url = `http://192.168.0.10:5000/api/news/${encodeURI(category)}`;

            try {
                //console.log(await axios.get(url));
                const { data } = await axios.get(url);
                const { articles } = data;
                setData(articles);
                //console.log(data);
            } catch (error) { }
        }
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <h4>Resultados para: {category}:</h4>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    {
                        datos.map((dato: any) =>
                            <NoticiasGridItem
                                items={dato}
                            />
                        )
                    }
                </Grid>
            </Grid>
        </Grid>
    )
}
