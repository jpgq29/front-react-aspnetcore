import React, { useState } from 'react'
import { AddCategory } from '../../AddCategory';
import { NoticiasGrid } from './NoticiasGrid';

export const Noticias = () => {

    const [categoria, setCategory] = useState('');
    return (
        <>
            <div>
                <AddCategory setCategories={setCategory} />
            </div>
            <div>
                <NoticiasGrid category={categoria} />
            </div>
        </>
    )
}
