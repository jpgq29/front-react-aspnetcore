import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

interface Props {
    data: any;   
}

export const TiempoGridItem = ({ data }: Props) => {
    const { weather, main, name } = data;
    const { temp, temp_min, temp_max, humidity } = main; 
    return (
        <Grid item xs={12}>
            <Card>
                <CardContent>                    
                    <Typography variant="h5" component="h2">
                       {name}
                    </Typography>                    
                    <Typography variant="body2" component="p">
                        Temperatura: {temp}                       
                    </Typography>
                    <Typography variant="body2" component="p">
                        Temperatura mínima: {temp_min}                       
                    </Typography>
                    <Typography variant="body2" component="p">
                        Temperatura máxima: {temp_max}                       
                    </Typography>
                    <Typography variant="body2" component="p">
                        Humedad: {humidity}                       
                    </Typography>
                    <Typography variant="body2" component="p">
                        Estado del tiempo: 
                        {
                            weather.map((weatherItem:any)=>
                                <span> {weatherItem.description}</span>
                            )
                        }                                          
                    </Typography>
                </CardContent>                
            </Card>
        </Grid>
    )
}
