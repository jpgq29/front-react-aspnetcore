import { useEffect, useState } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import { TiempoGridItem } from './TiempoGridItem';

interface Props {
    category: string
}

export const TiempoGrid = ({ category }: Props) => {

    const [datos, setData] = useState([]);
    const [renderizar, setRenderizar] = useState(false);    

    useEffect(() => {
        gifGrid();
    }, [category])

    const gifGrid = async () => {

        if (category.length > 0) {
            const url = `http://192.168.0.10:5000/api/weather/${encodeURI(category)}`;

            try {
                const { data } = await axios.get(url);
                setData(data);
                setRenderizar(true);
            } catch (error) { }
        }
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <h4>Resultados para: {category}:</h4>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={3}>
                    {renderizar ? <TiempoGridItem data={datos}/> : ''}
                </Grid>
            </Grid>
        </Grid>
    )
}
