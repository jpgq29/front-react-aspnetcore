import React, { useState } from 'react'
import { AddCategory } from '../../AddCategory';
import { TiempoGrid } from './TiempoGrid';

export const Tiempo = () => {

    const [categoria, setCategory] = useState('');
    return (
        <>
            <div>
                <AddCategory setCategories={setCategory} />
            </div>
            <div>
                <TiempoGrid category={categoria} />
            </div>
        </>
    )
}
