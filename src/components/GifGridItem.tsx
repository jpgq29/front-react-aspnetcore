import { ImageListItemBar, ListSubheader } from '@material-ui/core';
import ImageListItem from '@material-ui/core/ImageListItem';

interface Props {
    id: string,
    title: string,
    url: string
}

export const GifGridItem = ({ id, title, url }: Props) => {
    return (
        <>
            <ImageListItem key={id}> 
                <img src={url} alt={title} />
                <ImageListItemBar                    
                    title={title}
                />
            </ImageListItem>
        </>
    )
}
