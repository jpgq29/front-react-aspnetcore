import BottomNavigation from '@material-ui/core/BottomNavigation';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import HistoryIcon from '@material-ui/icons/History';
import TimerIcon from '@material-ui/icons/Timer';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import React from 'react'
import {
    Link
} from "react-router-dom";

export const Navigation = () => {
    const [value, setValue] = React.useState(0);
    return (
        <BottomNavigation
            value={value}
            onChange={(event, newValue) => {
                setValue(newValue);
            }}
            showLabels
        >
            <BottomNavigationAction
                component={Link}
                to="/"
                label="Noticias"
                icon={<AnnouncementIcon />}
            />
            <BottomNavigationAction
                component={Link}
                to="/tiempo"
                label="Estado del tiempo"
                icon={<TimerIcon />}
            />
            <BottomNavigationAction
                component={Link}
                to="/historial"
                label="Historial"
                icon={<HistoryIcon />}
            />
        </BottomNavigation>
    )
}
