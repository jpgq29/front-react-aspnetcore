import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { GifGridItem } from './GifGridItem';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import ListSubheader from '@material-ui/core/ListSubheader';

interface Props {
    category: string
}


export const GifGrid = ({ category }: Props) => {
    const [imagenes, setImages] = useState<any[]>();

    useEffect(() => {
        gifGrid();
    }, []);

    const gifGrid = async () => {
        const url = `https://api.giphy.com/v1/gifs/search?api_key=oW0XfyF7FfvpCBGRg6mbszIUdeZ9pMu7&q=${encodeURI(category)}&limit=10`;

        try {
            const { data } = await axios.get(url);
            //user = data.userDetails;
            console.log(data);

            let respuesta = data.data.map(({ id, title, images }: any) => {
                return {
                    id: id,
                    title: title,
                    url: images.downsized_medium.url
                };
            });
            setImages(respuesta);
            //console.log(resultado);

        } catch (error) {

        }
    }

    return (
        <div>
            <h3>{category}</h3>
            <ImageList rowHeight={200} gap={1}>               
                {
                    imagenes?.map((res, i) =>
                        <GifGridItem
                            key={i}
                            id={res.id}
                            title={res.title}
                            url={res.url}
                        />
                    )
                }
            </ImageList>
        </div>
    )
}
